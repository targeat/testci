# -*- coding: UTF-8 -*-

import sys
import sxtwl

lunar = sxtwl.Lunar()


def start():
    remindLunar = lunar.getDayByLunar(2019, 8, 25, True)
    print("Execute morning.py")


if __name__ == "__main__":
    start()
