# -*- coding: UTF-8 -*-

import sys
import os
import shutil
import matplotlib.font_manager


def __preCheckFont():
    path = (matplotlib.matplotlib_fname() + "/fonts/ttf/").replace("/matplotlibrc", "")
    files = os.listdir(path)
    if "SimHei.ttf" not in files:
        shutil.copyfile("./SimHei.ttf", path + "SimHei.ttf")
        matplotlib.font_manager._rebuild()


def start():
    print("Execute afternoon.py")
    print("字体检查之前")
    a = sorted([f.name for f in matplotlib.font_manager.fontManager.ttflist])
    for i in a:
        print(i)
    __preCheckFont()
    print("字体检查之后")
    a = sorted([f.name for f in matplotlib.font_manager.fontManager.ttflist])
    for i in a:
        print(i)


if __name__ == "__main__":
    start()
    print("ddddddd")
