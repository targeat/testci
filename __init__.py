# -*- coding: UTF-8 -*-

import sys

sys.path.append("./tool")
import morning
import afternoon


def main():
    print("test gitlab ci")
    morning.start()
    afternoon.start()


if __name__ == "__main__":
    main()
